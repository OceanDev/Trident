require('dotenv').config()

const fetch = require('node-fetch')
const HTMLParser = require('node-html-parser')
const fs = require('fs')

function sleep(milliseconds) {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

function prependToLine(string, content) {
  let newstring = ''
  string.split('\n').forEach((line) => {
    newstring += `${content}${line}\n`
  })
  return newstring
}

// fs.exists is deprecated, so we use fs.access instead (stolen from https://stackoverflow.com/a/35008327)
async function checkFileExists(file) {
  return fs.promises.access(file, fs.constants.F_OK)
    .then(() => true)
    .catch(() => false)
}

async function trackSubreddits() {
  while (true) {
    for (const sub of JSON.parse(process.env.SUBREDDITS)) {
      const text = await (await fetch(`https://old.reddit.com/r/${sub}`)).text()
      const parsed = HTMLParser.parse(text).querySelector('.interstitial-subreddit-description')

      // remove the subreddit name from the description
      parsed.removeChild(parsed.firstChild)
      
      const newDescription = parsed.text.trimRight()

      const subredditFileName = `subreddits/${sub}.txt`

      const subredditFileExists = await checkFileExists(subredditFileName)

      if (subredditFileExists) {
        const oldDescription = await fs.promises.readFile(subredditFileName, 'utf8')
        // check if they're different
        if (oldDescription !== newDescription) {
          // webhook post if they are
          const oldDescriptionDiff = prependToLine(oldDescription, '- ')
          const newDescriptionDiff = prependToLine(newDescription, '+ ')
          await fetch(process.env.WEBHOOK, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              username: 'Trident',
              content: `r/${sub}'s description has changed:\`\`\`diff\n${oldDescriptionDiff}${newDescriptionDiff}\`\`\``
            })
          })
          await fs.promises.writeFile(subredditFileName, newDescription)
        }
      } else
        await fs.promises.writeFile(subredditFileName, newDescription)
    }
    await sleep(process.env.DELAY)
  }
}

checkFileExists('subreddits').then(async (subredditsExists) => {
  if (!subredditsExists)
    await fs.mkdir('subreddits')
  await trackSubreddits()
})
